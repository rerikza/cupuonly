<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    public $timestamps = false;

    // public function prices()
    // {
    //     return $this->hasOne('App\')
    // }

    // nested eager loading
    public function tickets()
    {
        return $this->belongsTo('App\TicketModel','id','ticket_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class TicketModel extends Model
{
    protected $table = "ticket";
    public $timestamps = false;
    // use SoftDeletes;
    public function buku()
    {
        return $this->belongsTo('App\Buku');
    }

    // nested eager loading
    public function buku_nested()
    {
        return $this->belongsTo('App\Buku','id','type_id');
    }
    public function prices()
    {
        return $this->hasOne('App\Price','ticket_id','id');
    }
}

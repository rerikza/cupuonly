<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    public $timestamps = false;
    public function Role()
    {
        return $this->belongsToMany('App\Role', 'role_video', 'video_id','role_id');
    }
}

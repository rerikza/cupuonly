<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleVideo extends Model
{
    public $timestamps = false;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Buku extends Model
{
    protected $table = "buku";
    public $timestamps = false;
    use SoftDeletes;

    protected $appends = ['BukuJumlah'];

    protected $dates = ['deleted_at'];

    public function tickets()
    {
        return $this->hasMany('App\TicketModel','type_id','id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Buku','parent');
    }

    // menggunakan array recursif (super duper array)
    public function recursif()
    {
        return $this->hasOne('App\Buku', 'parent','id');
    }

    public function super()
    {
        return $this->recursif()->with('super');
    }

    // nsted eager loading
    public function tickets_nested()
    {
        return $this->hasMany('App\TicketModel', 'type_id','id');
    }

    // has Many Though
    public function prices_hasmanythough()
    {
        return $this->hasManyThrough(
            'App\Price',
            'App\TicketModel',
            'type_id',
            'ticket_id',
            'id',
            'id'
        );
    }

    // Accessor
    public function getBukuJumlahAttribute()
    {
        return strtolower("{$this->judul} {$this->jumlah_buku} buah ");
    }

    // Mutator
    public function setJudulAttribute($value)
    {
        $this->attributes['judul'] = ucwords($value);
    }

}

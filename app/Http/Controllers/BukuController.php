<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Buku;
use App\Http\Requests\StoreBlogPost;
use Carbon\Carbon;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::all();

        return view('buku',['buku'=> $buku]);
    }

    public function getdata() {
        $buku = Buku::all();
        return $buku;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $buku = Buku::create([
        //     ''
        // ]);


    }
    public function databanyak(){
        $data = Buku::all();

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBlogPost $request)
    {
        // $bukuvalidate = $request->validate([
        //     'judul' => 'required|unique:buku|max:5',
        //     'jumlah_buku' => 'required|numeric'
        // ]);

        // $validated = $request->validated();

        $cobac = "c";
        $judul = $request->input('judul');
        $jumlah_buku = $request->input('jumlah_buku');
        $buku = new Buku;
        $buku->judul = $judul;
        $buku->jumlah_buku = $jumlah_buku;
        $buku->save();

        $response = [
            'message' => 'data berhasil di tambah',
            'data' => $buku
        ];

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Buku::find($id);

        return view('percobaan2', ['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $judul = $request->input('judul');
        $jumlah_buku = $request->input('jumlah_buku');

        $buku = Buku::find($id);
        $buku->judul = $judul;
        $buku->jumlah_buku = $jumlah_buku;
        $buku->save();

        $response = [
            "data" => "data berhasil di update"
        ];

        return $response;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Buku::where('id', $id)->withTrashed()->first();
        // $delete->deleted_at = Carbon::now();
        // $delete->save();
        $delete->delete();
        $response = [
            'data' => 'sukses'
        ];
        return $response;
    }

    public function cobadatatable(Request $request)
    {

        // untuk menghitung data di dataabase
        $queryStringLength = $request->query('length');
        // // untuk membuat pagination
        $queryStringDraw = $request->query('draw');

        // untuk melihat inputan data yang diinput oleh user di datatable
        $queryStringSearch = $request->query('search');

        $queryStringStart = $request->query('start');


        // ambil data dati form search datatable
        $search = $queryStringSearch['value'];

        $buku = Buku::class;

        // mencari total data yang ada di database
        $total_buku = $buku::count();


        // menampilkan data sebelum di filter
        $data_buku = $buku::when($search, function($query, $search) {
                                return $query->where('judul', 'like',"%$search%");
                          })->withTrashed();

        // total data setelah filterd
        $total_filter_databuku = $data_buku->count();

        // data yang di filter
        $data_filter_databuku = $data_buku->offset($queryStringStart)
                                          ->limit($queryStringLength)
                                          ->withCount('tickets')
                                          ->withTrashed()
                                          ->orderBy('id','desc')
                                          ->get();


        // diubah ke array
        $dtbuku_jenis = $data_filter_databuku->toArray();


        // membuat nomor urutan di datatable
        $nilaiawal = $queryStringStart;
        $dtbuku_array = [];
        foreach( $dtbuku_jenis as $dtbuku_coba  ) {
            // data yang dipanggil di datatable
            $dtbuku_coba['no'] = ++$nilaiawal;
            $dtbuku_array[] =  $dtbuku_coba;
        }






        $response = [
            'draw' => $queryStringDraw,
            // ini untuk menghitung jumlah row
            'recordsTotal' => $total_buku,
            'recordsFiltered' =>$total_filter_databuku,
            'data' => $dtbuku_array
        ];

        // return response()->json($dtbuku);

        return $response;
    }


    public function cobaparent()
    {

        // array recursif
        // $jadiin = Buku::with('super')->get();
        // dd($jadiin->toArray());

        // Edger Loading tidak pakai with
        $jadiins = Buku::get();
        // foreach($jadiins as $jadiin) {
        //     echo $jadiin->tickets;
        //     // dd($jadiin->tickets);
        //     // dd($jadiin);
        // }

        // // Edger Loading pakai with
        // $jadiins = Buku::with('tickets')
        //                 ->get();
        // foreach($jadiins as $jadiin) {
        //     // $jadiin->toArray();
        //     echo "judul buku :".$jadiin->judul."<br>";
        //     foreach($jadiin->tickets as $cobaan) {
        //         // dd($cobaan->type_id);
        //         echo "tipe id :".$cobaan->type_id."<br>";
        //     }
        //     // echo $jadiin->jumlah_buku;
        //     // dd($jadiin->tickets);
        //     // // dd($jadiin);
        // }



        return view('cobaparent', ['jadiins' => $jadiins]);
    }



    public function hidup($id)
    {
        // mencari id
        $buku = Buku::where('id', $id)->withTrashed()->first();

        // mengembalikan data yang sudah di trashed
        $buku->restore();
        $response = [
            'data' => 'sukses'
        ];
        return $response;
    }
    public function matibener($id)
    {
        $buku = Buku::where('id', $id)->withTrashed()->first();

        // menghapus data dari database
        $buku->forceDelete();
        $response = [
            'data' => 'sukses'
        ];
        return $response;
    }


    public function nyicip(Request $request)
    {
        $queryStringId = $request->query('id');
        // $jadiins = Buku::with('tickets')
        //                 ->get();
        $jadiins = Buku::with('tickets')->where('id',$queryStringId)
        ->get();

        // $jadiins = Buku::where('id',$queryStringId)->get();

        return $jadiins;
    }

    public function nyicipahh()
    {
        $jadiins = Buku::with('tickets')->get();
        return $jadiins;
    }

}

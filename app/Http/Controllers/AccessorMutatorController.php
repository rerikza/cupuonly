<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;

class AccessorMutatorController extends Controller
{
    public function index()
    {
        return view('accessormutator');
    }
    public function getdata()
    {
        $buku = Buku::all();

        return $buku;
    }
}

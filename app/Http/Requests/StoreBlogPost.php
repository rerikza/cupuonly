<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBlogPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'judul' => 'required|max:5|unique:buku',
            'jumlah_buku' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'judul.required' => 'isiin dulu dong brooo',
            'judul.unique:buku' => 'ini ada pasangannya broooo',
            'jumlah_buku.required' => 'isiin dulu dong brooo'
        ];
    }
}

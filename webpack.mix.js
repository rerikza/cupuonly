const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .js('resources/js/nih.js', 'public/js')
    .js('resources/js/cobacoba.js', 'public/js')
    .js('resources/js/accessormutator.js', 'public/js')

mix.scripts([
    'resources/js/cobadatatable.js',
    'resources/js/cobaaxios.js'
    ], 'public/js/hasil_combine.js');


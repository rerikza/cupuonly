<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="/">coba <span class="sr-only">(current)</span></a>
        </li>
        {{-- <li class="nav-item">
          <a class="nav-link" href="/percobaan2">percobaan</a>
        </li> --}}
        <li class="nav-item">
          <a class="nav-link" href="/buku">Buku</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/cobaparent">Coba Parent</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/nested_eager">Nested Eager Loading</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/manytomany">Relation</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/accessormutator">accessormutator</a>
        </li>
      </ul>
    </div>
  </nav>

@extends('master')
@section('title', 'percobaan2')
@section('coba')


    <form action="/buku/update/{{$data->id}}" method="POST">
        {{csrf_field()}}
        <div class="form-group-row">
            <label for="judul" class="col-sm-2 col-form-label">Judul : </label>
            <input type="text" name="judul" class="form-control" value="{{$data->judul}}">
        </div>
        <div class="form-group-row">
            <label for="jumlah_buku" class="col-sm-2 col-form-label">Jumlah Buku :</label>
            <input type="number" name="jumlah_buku" class="form-control" value="{{$data->jumlah_buku}}">
        </div>
        <br>


        <button type="submit" class="btn btn-success">submit</button>
    </form>
@endsection

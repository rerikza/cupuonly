@extends('master')
@section('title','percobaan')
@section('coba')
<div class="progress" >
    <div class="progress-bar" id="loaded" role="progressbar" style="" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
</div>
<form action="/tiket/store" id="aa"  method="POST" enctype="multipart/form-data" >
   @csrf
    <div class="form-group-row coba1">
        <label for="tiket" class="col-sm-2 col-form-label">Input Type id : </label>
        <input type="text" name="tiket" class="form-control ">
    </div>
    <br>
    <div class="form-group-row">
        <div class="custom-file">
          <input type="file" class="custom-file-input" name="inputfile" id="inputfile">
          <label class="custom-file-label" for="inputfile">Choose file</label>

          {{-- <input type="file" name="inputfile" class="file-input" id=""> --}}
        </div>
    </div>
    <br>
    <button type="submit" class="btn btn-success" name="submit">submit</button>
</form>

@endsection

@push('script_content')
<script src="{{asset('js/nih.js')}}" defer></script>
@endpush

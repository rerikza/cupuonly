@extends('master')
@section('title','percobaan')
@section('coba')


    {{-- <h1 >ini Nested Eager</h1>
    @foreach($datas as $data)
        <ul>judul Buku :<b>{{$data->judul}}</b>
            {{-- @php echo $datas; @endphp --}}
            {{-- @foreach($data->tickets_nested as $datatype)
            <li> Tipe Buku :  {{$datatype->type_id}}</li>
                <li>Harga Buku : {{$datatype->prices['harga']}}</li>
                <br>
            @endforeach
        </ul>
    @endforeach --}}

    <h1>Ini Has Many Though</h1>
    @foreach($datas as $dts)
        <ul>Judul Buku : <b>{{$dts->judul}}</b></ul>
        @foreach ($dts->prices_hasmanythough as $dtsdts)
        <li>Harga Buku : <b>{{$dtsdts->harga}}</b></li>
        @endforeach

        {{-- @php dd($dts); @endphp --}}
    @endforeach

@endsection



@push('script_content')
    <script src="{{asset('js/cobacoba.js')}}"></script>
@endpush

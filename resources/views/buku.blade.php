@extends('master')
@section('title','Buku')
@section('coba')
    <form action="/buku/store" id="tambahdata" method="POST">
        {{csrf_field()}}
        <div class="form-group-row coba1">
            <label for="judul" class="col-sm-2 col-form-label">Judul : </label>
            <input type="text" name="judul" class="form-control judul1">
        </div>
        <div id="errorjudul" >

        </div>
        <div class="form-group-row coba2 ">
            <label for="jumlah_buku" class="col-sm-2 col-form-label">Jumlah Buku :</label>
            <input type="text" name="jumlah_buku" class="form-control">

            <div id="errorjumlah_buku" >

            </div>
        </div>
        <br>


        <button type="submit" class="btn btn-success" name="submit">submit</button>
    </form>
    <br>


    <br>

    {{-- ini coba datatable --}}
        <table class="table table-hover" id="cobadt">
            <thead>
                <tr class="thead-light">
                    <td>No </td>
                    <td>judul buku </td>
                    <td>jumlah buku</td>
                    <td>jumlah data</td>
                    <td>option</td>
                </tr>
            </thead>
            <tbody>

            </tbody>

        </table>

    {{-- tutup --}}

      <!-- Modal -->
      <div class="modal fade" id="modalku" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="#" id="modaldata" method="">
                    {{csrf_field()}}
                    <div class="form-group-row coba1" >
                            <label for="judul" class="col-sm-2 col-form-label">Judul : </label>
                            <input type="text" name="judul" class="form-control judul1" id="judul_modal">
                        </div>
                        <div class="form-group-row ">
                            <label for="jumlah_buku" class="col-sm-2 col-form-label">Jumlah Buku :</label>
                            <input type="number" name="jumlah_buku" class="form-control" id="jumlah_modal">
                        </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit"  class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
      </div>


@endsection

@push('script_content')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
{{-- <script src="js/cobascript.js"></script> --}}
{{-- <script src="js/cobadatatable.js"></script>
<script src="js/cobaaxios.js"></script> --}}

{{-- coba laravel mix --}}
    <script src="{{asset('js/hasil_combine.js')}}"></script>
@endpush

// mengidentifikasi jquery untuk halaman
$(document).ready(function() {

    // csrf untuk menggunakan ajax di laravel
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // ketika input dengan name judul di klik maka mengganti warna
    $('input[name=judul]').click(function() {
        $('.coba1').css('background-color','red')
    })

    // ketika input dengan name judul sudah tidak di klik itu akan menghilangkan css
    $('input[name=judul]').blur(function() {
        $('.coba1').css('background-color','')
    })

    // ketika input dengan name jumlah_buku di klik maka mengganti warna
    $('input[name=jumlah_buku]').click(function() {
        $('.coba2').css('background-color','red')
    })

    // ketika input dengan name jumlah_buku sudah tidak di klik itu akan menghilangkan css
    $('input[name=jumlah_buku]').blur(function() {
        $('.coba2').css('background-color','')
    })


    function loaddata(responses){
        $.each(responses, function(index, response) {
            $('#cobatabel tbody').append(`
                <tr>
                    <td>${index+1}</td>
                    <td id="judul_id" data-nama="${response.judul}" >${response.judul}</td>
                    <td id="jumlah_buku_id" data-nama="${response.jumlah_buku}">${response.jumlah_buku}</td>
                    <td>
                        <a href="#" class="badge badge-warning" id="edityuk" data-id="${response.id}">edit</a>
                        <a href="#" class="badge badge-danger" id="deleteyuk" data-id="${response.id}">delete</a>
                    </td>
                </tr>
            `)
        })
    }

    // ajax untuk mereload semua data dengan ajax
    $.ajax({
        method: "get",
        url: "/buku/getdata",
    })
    .done(function(responses) {
        // console.log(responses)
        // looping data
        loaddata(responses)
    })

    $('#tambahdata').submit(function() {
        event.preventDefault()
        // console.log('click')

        // ajax tanpa reload halaman
        $.ajax({
            method: "POST",
            url: "/buku/store",
            data: $('#tambahdata').serialize()
        })
        .done(function(responses) {
                // console.log(responses.data)
                $('tbody').empty();
                $.ajax({
                    method:"get",
                    url:"/buku/databanyak"
                })
                .done(function(responses) {
                    // looping dengan jquery
                    loaddata(responses)
                    // menghapus isi di tag inputan
                    $('input[name=judul]').val('')
                    $('input[name=jumlah_buku]').val('')

                })

            // })
        })

    })

    $(document).on("click", "#deleteyuk", function(event) {
        event.preventDefault();
        let no_id = $(event.target).data('id')
        // console.log(no_id)
        $.ajax({
            method: "get",
            url: "/buku/delete/"+no_id
        })
        .done(function(response) {
            $.ajax({
                method: "get",
                url: "/buku/getdata"
            })
            .done(function(responses) {
                $('#cobatabel tbody').empty()
                loaddata(responses)
            })
        })
    })

    $(document).on("click", "#edityuk", function(event) {
        $('#modalku').modal('show')
        let no_id = $(event.target).data('id')
        // console.log(no_id)
        // $('input[nama=judul]').val('')
        let data_judul = $(event.target)
                    .parent()
                    .siblings("#judul_id")
                    .data("nama")
        let data_jumlah_buku= $(event.target)
                    .parent()
                    .siblings("#jumlah_buku_id")
                    .data("nama")
        // console.log(data_judul)
        // console.log(data_jumlah_buku)
        $('#judul_modal').val(data_judul)
        $('#jumlah_modal').val(data_jumlah_buku)
        $('#modaldata').attr('action','/buku/update/'+no_id )
    })

    $('#modaldata').submit(function(event) {
        event.preventDefault()
        // let no_id = $(event.target).data('id')

        // console.log(no_id)
        $.ajax({
            method:"post",
            url: $('#modaldata').attr('action'),
            data: $('#modaldata').serialize()
        })
        .done(function(responses) {
            console.log(responses)
            $('#cobatabel tbody').empty()
            // loaddata(responses)

        })
        .always(function() {
            $('#modalku').modal('hide')
            $.ajax({
                method: "get",
                url: "/buku/getdata",
            })
            .done(function(responses) {
                // console.log(responses)
                // looping data
                loaddata(responses)
            })
        })
    })



    // minta dari iqbal



})

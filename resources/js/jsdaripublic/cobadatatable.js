$(document).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#cobadt").DataTable({
        serverSide: true,

        ajax: {
            method: "get",
            url: "/bukudatatable"
        },
        columns: [
            { data: "no" },
            { data: "judul" },
            { data: "jumlah_buku" },
            { data: "tickets_count" },
            { data: "id" }
        ],
        columnDefs: [
            {
                targets: [4],
                render: function(data, type, row, meta) {
                    // untuk melihat data yang sesuai kolom target
                    // console.log(data)
                    // console.log('<br>')

                    // untuk melihat display
                    // console.log(type)
                    // console.log('<br>')

                    // untuk melihat isi dalam baris ada apa saja
                    // console.log(row)
                    // console.log('<br>')

                    // untuk melihat settingandari baris dan datatable
                    // console.log(meta)
                    return `
                            <a href="#" class="badge badge-warning" id="edityuk" data-id="${data}" >edit</a>
                            <a href="#" class="badge badge-danger" id="deleteyuk" data-id="${data}">delete</a>
                            <a href="#" class="badge badge-primary" id="hidupkan" data-id="${data}">hidupkan</a>
                            <a href="#" class="badge badge-danger" id="matikan" data-id="${data}">Matikan</a>
                        `

                    // return `
                    //             <a href="" class="badge badge-warning">kucing</a>
                    //         `;
                }
            },
            {
                targets : [3,2],
                orderable : false
            }

        ]
    });

    // cobadatatable()


    $('#tambahdata').submit(function() {
        event.preventDefault()
        // console.log('click')

        // ajax tanpa reload halaman
        $.ajax({
            method: "POST",
            url: "/buku/store",
            data: $('#tambahdata').serialize()
        })
        .done(function(responses) {

                // $('tbody').empty();
                // cobadatatable()
                $("#cobadt").DataTable().ajax.reload()
                $('input[name=judul]').val('')
                $('input[name=jumlah_buku]').val('')
                Swal.fire({
                    type: 'success',
                    text: 'Sukses Tambah data!',
                  })
        })

    })

    $(document).on("click", "#deleteyuk", function(event) {
        event.preventDefault()

        let no_id = $(event.target).data('id');
        $.ajax({
            method:"get",
            url: "/buku/delete/"+no_id
        })
        .done(function(responses) {
            $("#cobadt").DataTable().ajax.reload()

        })
    })

    $(document).on("click", "#edityuk", function(event) {
        $('#modalku').modal('show')

        // mengambil data id pada tomol yang akan kita klik
        let no_id = $(event.target).data('id')

        //ambil data row yang kita klik
        let index = $(event.target).parents('tr').index()

        // menampilkan data row yang kita klik
        let data = $("#cobadt").DataTable().rows(index).data()

        // menampilkan data di form modal pada edit data
        $('#judul_modal').val(data[0].judul)
        $('#jumlah_modal').val(data[0].jumlah_buku)

        // membuat url untuk ajax di modal dengan menarik atribut
        $('#modaldata').attr('action','/buku/update/'+no_id)
    })

    $("#modaldata").submit(function(event) {
        event.preventDefault()
        $.ajax({
            method :"post",
            url:$("#modaldata").attr('action'),
            data:$("#modaldata").serialize()
        })
        .done(function(responses) {
            // meload datatable
            $("#cobadt").DataTable().ajax.reload()
        })
        .always(function() {
            // menghapus dan menghilangkan modal setelah digunakan
            $('#modalku').modal('hide')
        })
    })

    $(document).on("click", "#hidupkan", function(event) {
        event.preventDefault()

        let no_id = $(event.target).data('id');
        $.ajax({
            method:"get",
            url: "/buku/hidup/"+ no_id
        })
        .done(function(responses) {
            $("#cobadt").DataTable().ajax.reload()
        })
    })
    $(document).on("click", "#matikan", function(event) {
        event.preventDefault()

        let no_id = $(event.target).data('id');
        $.ajax({
            method:"delete",
            url: "/buku/matibener/"+ no_id
        })
        .done(function(responses) {
            $("#cobadt").DataTable().ajax.reload()
        })
    })






});

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('coba');
});

// Route::get('/coba', function(){
//     return view('coba');
// });

// Route::get('/percobaan2', function(){
//     return view('percobaan2');
// });

Route::get('/buku','BukuController@index');
Route::get('/buku/getdata','BukuController@getdata');

Route::get('/buku/databanyak','BukuController@databanyak');

Route::post('/buku/store','BukuController@store');
Route::get('/buku/delete/{id}','BukuController@destroy');
Route::get('/buku/edit/{id}','BukuController@edit');
Route::post('/buku/update/{id}','BukuController@update');


// ajax datatable
Route::get('/bukudatatable',[
    'uses' => 'BukuController@cobadatatable'
]);


// input tiket
Route::get('/', [
    'uses' => 'TicketController@index'
]);
Route::post('/tiket/store', [
    'uses' => 'TicketController@store'
]);


Route::get('/cobaparent',[
    'uses' => 'BukuController@cobaparent'
]);

Route::get('/buku/hidup/{id}', [
    'uses' => 'BukuController@hidup'
]);

Route::delete('/buku/matibener/{id}', [
    'uses' => 'BukuController@matibener'
]);


Route::get('/nyicipahh','BukuController@nyicipahh');
Route::get('/nyicip','BukuController@nyicip');

// nested eager
Route::get('/nested_eager','PriceController@index');


// many to many
Route::get('/manytomany','RelationController@index');

Route::get('/kucintajs','RelationController@kucintajs');


Route::get('/accessormutator','AccessorMutatorController@index');
Route::get('/accessormutator/getdata','AccessorMutatorController@getdata');


